(function() {
    $('html').addClass('js');

    //  CREATE AN OBJECT LITERAL
    var contactForm = {

        container: $('.my-contact-form'),

        ola: function() {
            alert("ola");
        },

        config: {
            effect: 'slideToggle',
            speed: 2000
        },

        // can reference like
        // contactForm.container OR
        // contactForm['container']

        init: function(config) {
            // console.log(this);

            $.extend(this.config, config);

            $('<button></button>', {
                text: "Contact Me"
            })
                .insertAfter('.page')
                .on('click', this.showForm);
        },

        showForm: function() {
            console.log("showing");
            // console.log($(this));

            var contactF = contactForm,
                container = contactForm.container,
                config = contactForm.config;

            if (container.is(':hidden')) {
                console.log("Container is hidden");
            }

            contactF.closeForm.call(container);
            // contactForm.container.show(); change this for config
            container[config.effect](config.speed);
        },

        closeForm: function() {
            var $this = $(this);

            if ($this.find('span.close').length) return;

            $('<span></span>', {
                text: 'Close',
                class: 'close'
            })
                .appendTo('.my-contact-form')
                .on('click', function() {
                    $this[contactForm.config.effect](contactForm.config.speed);
                });
        },

        doStuff: function() {
            //            alert("I came from doStuff!");
        }

    };

    contactForm.init({
        effect: 'fadeToggle',
        speed: 100

    });
    contactForm.doStuff();
})();